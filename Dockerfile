FROM docker.io/library/centos:8

ENV ANSBILE_INVENTORY=/multinode

RUN dnf install -y python3-devel libffi-devel gcc openssl-devel python3-libselinux python3-pip openssh-clients \
    && python3 -m pip install -U pip \
    && python3 -m pip install ansible kolla-ansible \
    && mkdir -p /etc/kolla \
    && cp -r /usr/local/share/kolla-ansible/etc_examples/kolla/* /etc/kolla/ \
    && cp /usr/local/share/kolla-ansible/ansible/inventory/* / \
    && yum clean -y all \
    && rm -rf /var/cache/yum/*

COPY ansible.cfg /etc/ansible/ansible.cfg

ENTRYPOINT ["/bin/bash"]
